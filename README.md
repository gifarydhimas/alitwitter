# Ali Twitter

Twitter-like web app which can:

1. Create a tweet
2. Delete a tweet
3. List tweets

## Environment Setup

- Ruby 2.6.5
- Bundler 2.1.4
- NodeJS 12.15.0
- Yarn 1.22.4
- PostgreSQL 10.12

Install all dependency using bundle in root of directory project.
```bash
bundle install
```

then

```bash
yarn
```

## Run Test

To run unit test, execute the following command:

```bash
bundle exec rails test
```

To run system test (browser test), execute the following command:

```bash
bundle exec rails test:system
```

## Run Instruction

### 1. Configure and Migrate the database

Please modify `config/database.yml` with your PostgreSQL configuration.

```bash
bundle exec rails db:migrate
```

### 2. Run the server

```bash
bundle exec rails server
```

### 3. Open the following link in your browser

> [http://localhost:3000/tweets](http://localhost:3000/tweets)

## Run in Docker

### Building the Docker Image

To build a Docker Image, execute the following command:

```
docker image build -t gifff/alitwitter:1.0 .
```

### Spawn a container

```
docker container run --name alitwitter -d -p 3000:3000 gifff/alitwitter:1.0
```

Migrate the databse using following command:

```
docker container exec -it alitwitter bundle exec rails db:migrate
```
